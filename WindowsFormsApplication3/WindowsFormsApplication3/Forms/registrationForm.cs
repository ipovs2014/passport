﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApplication3
{
    public partial class registrationForm : Form
    {
        SqlConnection conn;
        public registrationForm()
        {
            InitializeComponent();
        }
        public void registrationForm_Load(object sender, EventArgs e)
        {
            string connString = @"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\Users\Александр\Desktop\WindowsFormsApplication3\WindowsFormsApplication3\example_DB.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True";      
            conn = new SqlConnection(connString);
           
            try
            {
                conn.Open();
            }
            catch (SqlException se)
            {
                MessageBox.Show(se.Message);
            }
        }
          
        public void createsuserButton_Click(object sender, EventArgs e)
        {

            if (confirmpassTextbox.Text != passwordTextbox.Text)
            {
                MessageBox.Show("Пароли не совпадают", "Внимание");
                return;
            }

            if (loginTextbox.Text== String.Empty|| passwordTextbox.Text==String.Empty)
            {
                MessageBox.Show("Не заполнены требуемые данные", "Внимание");
                return;
            }

                 // Чтобы осуществить внесение данных в БД, вызовем хранимую процедуру
                 SqlCommand command = conn.CreateCommand();
                 command.CommandType = CommandType.StoredProcedure;
                 command.CommandText = "StoredProcedure3";
                 // Передадим ей аргументы
                 command.Parameters.AddWithValue("@Login", loginTextbox.Text);
                 command.Parameters.AddWithValue("@Password", passwordTextbox.Text);         
                 // Инициализация возвращаемого значения хранимой процедуры
                 SqlParameter retValue = command.Parameters.Add("@return", SqlDbType.Int);
                 retValue.Direction = ParameterDirection.ReturnValue;
                                  
                try{
                     command.ExecuteNonQuery();
                     int pr = (int)retValue.Value; // возвращенное значение хр.процедуры
                     switch (pr)
                     {
                         case -100:
                             MessageBox.Show("Такой пользователь существует", "Внимание");
                             break;
                         case 100:
                             MessageBox.Show("Регистрация прошла успешно", "Внимание");
                             break;
                        
                     }
                
                }
                catch (SqlException se)
                {
                    MessageBox.Show(se.Message);
                }
             
        }
    }
}
